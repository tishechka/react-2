import React from 'react';
import PropTypes from 'prop-types';
function Star({item , deleteStarList}) {
    return (
        <>
            <h2 className="star-saved">You liked</h2>
            {
                item.map((el) => (
                    <div className="buy-item" key={el.id}>
                        <img src={"images/" + el.img} width="120" height="75" alt="star"/>
                        <h2>{el.name}</h2>
                        <b>{el.price}</b>
                    </div>
                ))
            }
        </>
    );
}

Star.propTypes = {
    item : PropTypes.array.isRequired,
  
    }
export default Star;
