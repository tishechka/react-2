import React from 'react';
import { MdDeleteOutline } from "react-icons/md";
import PropTypes from 'prop-types';
function Oders({item ,deleteOder }) {
    return (
        <>
            {
                item.map((el)=>(
                    <div className="buy-item" key={el.id}>
                        <img src={"images/" + el.img} width="120" height="75" alt="oders"/>
                    <h2 >{el.name}</h2>
                        <b>{el.price}</b>
                        <MdDeleteOutline className='delete' onClick={()=> {deleteOder(el)}}/>
                    </div>
                ))
            }
        </>
    );
}
Oders.propTypes = {
    item : PropTypes.array.isRequired,
    }

export default Oders;
