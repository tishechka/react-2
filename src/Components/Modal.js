import React from 'react';
import PropTypes from 'prop-types';
function Modal({ Modal_text, modalToogle, chosee, mainOder, clearMainOder }) {
    return (
        <div>
            <div id="blur" className="blur" onClick={(e) => (
                e.target.id === "blur" && (modalToogle(), clearMainOder())
            )}>
                <div className="modal">
                    <p className="modal__txt">{Modal_text}</p>
                    <div className="modal__btn-wrapper">
                        <button onClick={() => {
                            chosee(...mainOder)
                            modalToogle();

                        }} >OK</button>
                        <button onClick={() => {
                            modalToogle();
                            clearMainOder();
                           
                        }}>Cancel</button>
                    </div>

                </div>
            </div>
        </div>
    );
}
Modal.propTypes = {
    Modal_text: PropTypes.string,
    titleName: PropTypes.string,
    chosee: PropTypes.func.isRequired,
    clearMainChoose: PropTypes.func,
    modalToogle: PropTypes.func,
    mainOder: PropTypes.array.isRequired,

}
export default Modal;

