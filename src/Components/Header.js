import React, {useState} from 'react';
import {MdOutlineShoppingCart} from "react-icons/md";
import Oders from "./Oders";
import {FaRegStar} from "react-icons/fa";
import Star from "./Star"
import PropTypes from 'prop-types';

function Header({oders, starList, deleteOder, deleteStarList}) {
    const [cardOpen, setCardOpen] = useState(false)
    const [cardOpenStar, setCardOpenStar] = useState(false)
    const cardToogle = () => {
        setCardOpen(!cardOpen)
    }
    const showBuy = (props, deleteOder) => {
        return (
            <>
                <Oders item={props} deleteOder={deleteOder}/>
            </>
        )
    }
    const showStar = (props, deleteStarList) => {
        return (
            <>
                <Star item={props} deleteStarList={deleteStarList}/>
            </>
        )
    }
    const showNoting = (text) => {
        return (
            <h2 className="showNoting">{text}</h2>
        )
    }
    return (
        <div className="header">
            <li>All Colections</li>
            <span className="logo">Fragrance for soul</span>
            <ul>
                <p className='oder_counter'>{oders.length}</p>
                <MdOutlineShoppingCart className={`icon-buy ${cardOpen && `active`}`} onClick={() => cardToogle()}/>
                <p className='star_counter'> {starList.length}</p>
                <FaRegStar className={` ${cardOpenStar && `star-active`}`}
                           onClick={() => setCardOpenStar(!cardOpenStar)}/>
            
                <li>Contact us</li>
            </ul>
            {cardOpen && <div className="showBuy">
                {oders.length > 0 ? showBuy(oders, deleteOder,) : showNoting(`No item yet`)}</div>
            }
            {cardOpenStar && <div className="showStar">
                {starList.length > 0 ? showStar(starList, deleteStarList) : showNoting(`You don't liked any item yet`)}</div>
            }
        </div>
    );
}

Header.propTypes = {
    oders: PropTypes.array.isRequired,
    starList: PropTypes.array.isRequired,
    deleteOder: PropTypes.any.isRequired,
    deleteStarList: PropTypes.any.isRequired,
}
export default Header;
