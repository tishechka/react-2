import CardItem from "./CardItem";
import PropTypes from 'prop-types';
function Card({ Item, Choose, modalToogle, modal, clearMainChoose, mainChoose, mainoders, starListAdd, deleteStarList  }) {
  
    return (

        <div className="card">
            {Item.map((el) => (
                <CardItem key={el.id}
                    itemCard={el}
                    Chose={Choose}
                    modalToogle={modalToogle}
                    modal={modal}
                    clearMainChoose={clearMainChoose}
                    mainChoose={mainChoose}
                    mainoders={mainoders}
                    starListAdd={starListAdd}
                    deleteStarList={deleteStarList}
                   />
            ))
            }
        </div>
    );
}
Card.propTypes = {
    Item : PropTypes.array.isRequired,
    mainoders: PropTypes.array.isRequired,
    modal: PropTypes.bool,
    Choose: PropTypes.func.isRequired,
    clearMainChoose: PropTypes.func.isRequired,
    modalToogle: PropTypes.func,
    starListAdd: PropTypes.func.isRequired,
    deleteStarList: PropTypes.func.isRequired,
    mainChoose:PropTypes.func.isRequired,
    }
export default Card;
