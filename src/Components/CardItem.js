import React, { useState, useEffect } from 'react';
import Modal from "./Modal";
import { FaRegStar } from "react-icons/fa";
import PropTypes from 'prop-types';
function CardItem({ itemCard, Chose, modalToogle, modal, clearMainChoose, mainChoose, mainoders, starListAdd, deleteStarList  }) {
    const [addStar, setaddStar] = useState(false)
 
    const change = () => {
        setaddStar(!addStar)

    }
    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('star'));
        if (starData) {
          starData.forEach((element) => {
            if (element.id === itemCard.id) {
              change();
            }
          });
        }
      }, []);

    return (<div className="card_item">
        <h2 className={"sale"}>SALE</h2>
        <h2 > {itemCard.name}</h2>
        <img src={"images/" + itemCard.img} width="160" height="155" alt="photoww" />
        <p>Price : {itemCard.price}</p>
        <button   onClick={() => {              
             modalToogle();
             mainChoose(itemCard)
        }}>Order</button>
        <FaRegStar className={`star ${(addStar) && `star-active`}`} onClick={() => {
           change();
           !addStar ?  starListAdd(itemCard) : deleteStarList(itemCard)
        }} />
        {modal && <Modal Modal_text="Do you want to add this item in buy list?"
            modalToogle={modalToogle}
            chosee={Chose}
            mainOder={mainoders}
            clearMainOder={clearMainChoose}
         
           />}
    </div >

    );
}
CardItem.propTypes = {
    itemCard : PropTypes.object.isRequired,
    mainoders: PropTypes.array.isRequired,
    modal: PropTypes.bool.isRequired,
    Chose: PropTypes.func.isRequired,
    clearMainChoose: PropTypes.func.isRequired,
    modalToogle: PropTypes.func,
    starListAdd: PropTypes.func.isRequired,
    deleteStarList: PropTypes.func.isRequired,
    mainChoose:PropTypes.func.isRequired,
    
    }
export default CardItem
