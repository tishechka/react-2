import './Main_styles.scss'
import React, { useEffect, useState } from "react";
import Card from "./Components/Card";
import Header from "./Components/Header";

function App() {
    const [mainoders, SetMainOders] = useState([])
    const [oders, setOders] = useState([])
    const [item, setItem] = useState()
    const [modal, setModal] = useState(false)
    const [starList, setStarList] = useState([])

    useEffect(() => {
        setOders(JSON.parse(localStorage.getItem(`oders`)) || [])
    },[])

    useEffect(() => {
        localStorage.setItem(`oders`, JSON.stringify(oders))
    },[oders])

    useEffect(() => {
        setStarList(JSON.parse(localStorage.getItem(`star`)) || [])
    },[])

    useEffect(() => {
        localStorage.setItem(`star`, JSON.stringify(starList))
    },[starList])

    const deleteOder = (item) => {
        setOders([...oders].filter((el) => el.id !== item.id))
    }

    const deleteStarList = (item) => {
        setStarList([...starList].filter((el) => el.id !== item.id))
    }
    const starListAdd = (item) => {
        let add = false;
        starList.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add &&
            setStarList([...starList, item]);
    }
    const modalToogle = () => {
        setModal(!modal)
    }

    const clearMainChoose = (item) => {
        SetMainOders([])
    }
    const mainChoose = (item) => {
        SetMainOders([item]);

    }
    const Choose = (item) => {
        let add = false;
        oders.forEach((el) => {
            if (el.id === item.id) {
                add = true
            }
        })
        !add && setOders([...oders, item]);
    }

    useEffect(() => {
        const fetchItems = async () => {
            try {
                const response = await fetch(`/getItem.json`);
                const data = await response.json()
                setItem(data.item)
            } catch (er) {
                console.warn(er)
            }
        };
        fetchItems();
    }, [])
    return (<>
        {item && <div className="App">
            <header>
                <Header oders={oders}
                 starList={starList} 
                 deleteOder={deleteOder}
                 deleteStarList={deleteStarList} />
            </header>
            <main>
                <Card Item={item}
                    Choose={Choose}
                    modalToogle={modalToogle}
                    modal={modal}
                    clearMainChoose={clearMainChoose}
                    mainChoose={mainChoose}
                    mainoders={mainoders}
                    starListAdd={starListAdd}
                    starList={starList}
                    deleteStarList={deleteStarList}
                    />
            </main>

        </div>}
    </>
    );
}


export default App;

